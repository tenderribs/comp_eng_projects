// This program will check your assignments (a further check for forbidden instructions happens after this program).
// Feel free to modify this file to help you in debugging your assembly code.
// Note that if you modify this file, there may be mismatches between the grade that you see from your local tests, and the grade that you will receive.

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

extern long long int assignment_2_2(int*);

static inline int test_assignment_2_2(int num_reps) {
    int is_success = 1;
    int arr[1000];
    for (int i = 0; i < num_reps; i++) {
        int zero_index = rand() % 1000;
        for (int j = 0; j < 1000; j++)
            if (j == zero_index)
                arr[j] = 0;
            else
                while (!arr[j])
                    arr[j] = rand() % 1024;
        int got = assignment_2_2(arr);
        if(got != zero_index) {
            printf("assignment_2_2 failed at addr a=0x%p. Got 0x%llx, expected 0x%llx. Table was:\n", arr, got, zero_index);
            printf("[");
            for (int j = 0; j < 1000; j++)
                printf(" %llx ", arr[j]);
            printf("]\n");
            is_success = 0;
            break;
        }
    }
    return is_success;
}


int main(int argc, char **argv) {
    srand(0);
    
    int num_reps = 100;

    int success = test_assignment_2_2(num_reps) == 1;
    printf("\nsuccess: %d\n", success);
}