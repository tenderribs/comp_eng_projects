// This program will check your assignments (a further check for forbidden instructions happens after this program).
// Feel free to modify this file to help you in debugging your assembly code.
// Note that if you modify this file, there may be mismatches between the grade that you see from your local tests, and the grade that you will receive.

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

extern long long int assignment_2_3(int*, int);

static inline int test_assignment_2_3(int num_reps) {
    int is_success = 1;
    int32_t my_int;
    int32_t my_int_location;

    for (int i = 0; i < num_reps; i++) {
        int is_big_endian = rand() & 1;
        my_int = rand();
        if (is_big_endian) {
            *(uint8_t*)(&my_int_location)       = (my_int & 0xFF000000) >> 24;
            *(((uint8_t*)(&my_int_location))+1) = (my_int & 0x00FF0000) >> 16;
            *(((uint8_t*)(&my_int_location))+2) = (my_int & 0x0000FF00) >> 8;
            *(((uint8_t*)(&my_int_location))+3) = (my_int & 0x000000FF);
        } else {
            *(uint8_t*)(&my_int_location)       = (my_int & 0x000000FF);
            *(((uint8_t*)(&my_int_location))+1) = (my_int & 0x0000FF00) >> 8;
            *(((uint8_t*)(&my_int_location))+2) = (my_int & 0x00FF0000) >> 16;
            *(((uint8_t*)(&my_int_location))+3) = (my_int & 0xFF000000) >> 24;
        }

        int32_t got = assignment_2_3(&my_int_location, is_big_endian);
        if(got != my_int) {
            printf("assignment_2_3 failed for a1=%d. Got 0x%llx, expected 0x%llx\n", is_big_endian, got, my_int);
            is_success = 0;
            break;
        }
    }
    return is_success;
}


int main(int argc, char **argv) {
    srand(0);
    
    int num_reps = 100;

    int success = test_assignment_2_3(num_reps) == 1;
    printf("\nsuccess: %d\n", success);
}