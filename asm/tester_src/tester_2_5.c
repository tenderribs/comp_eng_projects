// This program will check your assignments (a further check for forbidden instructions happens after this program).
// Feel free to modify this file to help you in debugging your assembly code.
// Note that if you modify this file, there may be mismatches between the grade that you see from your local tests, and the grade that you will receive.

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

extern int64_t assignment_2_5(int64_t (*fun_ptr)(void*, int64_t), int64_t);

static int expected_recursive_call_args[40];
static int index_in_recursive_table;
static int assignment_2_5_success;
int recursive_call_2_5(int recursive_arg) {
    if (expected_recursive_call_args[index_in_recursive_table] < 0) {
        printf("Error: in assignment 2_5. Too many recursive calls. Aborting the tests.\n");
        assignment_2_5_success = 0;
        exit(0);
    }
    if (recursive_arg != expected_recursive_call_args[index_in_recursive_table]) {
        printf("Error: recursive call of assignment 2_5 provided wrong a1 (intermediate result) at step %d (first step is 0). Got 0x%llx, expected 0x%llx.\n", index_in_recursive_table, recursive_arg, expected_recursive_call_args[index_in_recursive_table]);
        assignment_2_5_success = 0;
    }
    index_in_recursive_table++;
    return assignment_2_5((int64_t (*)(void*, int64_t))recursive_call_2_5, recursive_arg);
}
static void fill_expected_recursive_call_args(int64_t initial_n) {
    for (int i = 0; i < initial_n+1; i++) {
        expected_recursive_call_args[i] = initial_n-i;
    }
    for (int i = initial_n+1; i < 40; i++) {
        expected_recursive_call_args[i] = -1;
    }
}

static inline int64_t sum(int64_t n) {
    int64_t ret = 0;
    for (int64_t i = 1; i <= n; i++) {
        ret += i;
    }
    return ret;
}

static inline int test_assignment_2_5(int num_reps) {
    assignment_2_5_success = 1;
    for (int i = 0; i < num_reps; i++) {
        index_in_recursive_table = 0;
        int a = i % 30;
        printf("Repetition id: %d with val %d.\n", i, a);
        fill_expected_recursive_call_args(a);
        int64_t ret = recursive_call_2_5(a);
        // Check that the function has been called the right number of times.
        if (index_in_recursive_table != a+1) {
            printf("Wrong number of recursive calls.\n");
            return 0;
        }
        printf("Correct number of recursive calls.\n");

        int64_t expected_sum = sum(a);
        if (ret != expected_sum) {
            printf("Wrong return value for a0=%llp, a1=%llx. Got 0x%llx, expected 0x%llx.\n", recursive_call_2_5, a, ret, expected_sum);
            return 0;
        }
    }
    return assignment_2_5_success;
}


int main(int argc, char **argv) {
    srand(0);
    int num_reps = 100;

    int success = test_assignment_2_5(num_reps) == 1;
    printf("\nsuccess: %d\n", success);
}