// This program will check your assignments (a further check for forbidden instructions happens after this program).
// Feel free to modify this file to help you in debugging your assembly code.
// Note that if you modify this file, there may be mismatches between the grade that you see from your local tests, and the grade that you will receive.

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

extern long long int assignment_1_4();

static inline int test_assignment_1_4() {
    int64_t got = assignment_1_4();
    int is_success = 0xbadcab1eLL == got;
    if (!is_success)
        printf("assignment_1_4 failed Got 0x%llx, expected 0x%llx.\n", got, 0xbadcab1eLL);
    return is_success;
}

int main(int argc, char **argv) {
    srand(0);
    
    int num_reps = 100;

    int success = test_assignment_1_4(num_reps) == 1;
    printf("\nsuccess: %d\n", success);
}