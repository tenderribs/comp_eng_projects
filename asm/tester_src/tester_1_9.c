// This program will check your assignments (a further check for forbidden instructions happens after this program).
// Feel free to modify this file to help you in debugging your assembly code.
// Note that if you modify this file, there may be mismatches between the grade that you see from your local tests, and the grade that you will receive.

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

extern long long int assignment_1_9(int*, int);

static inline int test_assignment_1_9(int num_reps) {
    int is_success = 1;
    int arr[1000];
    for (int i = 0; i < num_reps; i++) {
        int id_in_arr = rand() % 1000;
        int *a = (arr + id_in_arr);
        int before = *a;
        assignment_1_9(arr, id_in_arr);
        if(*a != before+1) {
            printf("assignment_1_9 failed at addr a=0x%p and offset b=0x%llx. Got 0x%llx, expected 0x%llx.\n", arr, id_in_arr, *a, before+1);
            is_success = 0;
            break;
        }
    }
    return is_success;
}


int main(int argc, char **argv) {
    srand(0);

    int num_reps = 100;

    int success = test_assignment_1_9(num_reps) == 1;
    printf("\nsuccess: %d\n", success);
}