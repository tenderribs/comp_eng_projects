// This program will check your assignments (a further check for forbidden instructions happens after this program).
// Feel free to modify this file to help you in debugging your assembly code.
// Note that if you modify this file, there may be mismatches between the grade that you see from your local tests, and the grade that you will receive.

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

extern long long int assignment_2_1(int);

static int fibonacci(int n) {
    int a0 = 0;
    int a1 = 1;

    if (n == 0)
        return 0;

    while (--n) {
        int t = a1 + a0;
        a0 = a1;
        a1 = t;
    }
    return a1;
}

static inline int test_assignment_2_1(int num_reps) {
    int is_success = 1;
    for (int i = 0; i < num_reps; i++) {
        int a = rand() % 48;
        int got = assignment_2_1(a);
        int expected = fibonacci(a);
        if(got != expected) {
            printf("assignment_2_1 failed for a=0x%llx. Got 0x%lx, expected 0x%lx.\n", a, got, expected);
            is_success = 0;
            break;
        }
    }
    return is_success;
}


int main(int argc, char **argv) {
    srand(0);
    
    int num_reps = 100;

    int success = test_assignment_2_1(num_reps) == 1;
    printf("\nsuccess: %d\n", success);
}