// This program will check your assignments (a further check for forbidden instructions happens after this program).
// Feel free to modify this file to help you in debugging your assembly code.
// Note that if you modify this file, there may be mismatches between the grade that you see from your local tests, and the grade that you will receive.

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

extern int64_t assignment_1_3(int64_t);

static inline int test_assignment_1_3(int num_reps) {
    int is_success = 1;
    for (int i = 0; i < num_reps; i++) {
        int64_t a = (((int64_t) rand()) << 32) | rand();
        int64_t got = assignment_1_3(a);
        int64_t expected = ~a;
        if(got != expected) {
            printf("assignment_1_3 failed for a=0x%llx. Got 0x%llx, expected 0x%llx.\n", a, got, expected);
            is_success = 0;
            break;
        }
    }
    return is_success;
}

int main(int argc, char **argv) {
    srand(0);
    
    int num_reps = 100;

    int success = test_assignment_1_3(num_reps) == 1;
    printf("\nsuccess: %d\n", success);
}