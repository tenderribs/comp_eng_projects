// This program will check your assignments (a further check for forbidden instructions happens after this program).
// Feel free to modify this file to help you in debugging your assembly code.
// Note that if you modify this file, there may be mismatches between the grade that you see from your local tests, and the grade that you will receive.

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

extern long long int assignment_2_6(int16_t*, int, int, int16_t*, int, int16_t*);

#define MAX_MATR_SIDE_2_6 15
#define MAX_MATR_TOTAL_SIZE_2_6 (MAX_MATR_SIDE_2_6*MAX_MATR_SIDE_2_6)
static int M0_width;
static int M0_height;
static int M1_width;
static int M1_height;
static int16_t M0[MAX_MATR_TOTAL_SIZE_2_6];
static int16_t M1[MAX_MATR_TOTAL_SIZE_2_6];
static int16_t M2[MAX_MATR_TOTAL_SIZE_2_6];
static int16_t M2_ref[MAX_MATR_TOTAL_SIZE_2_6];

void populate_matrices_2_6() {
    for (int i = 0; i < MAX_MATR_SIDE_2_6; i++)
        for (int j = 0; j < MAX_MATR_SIDE_2_6; j++) {
            M0[i*MAX_MATR_SIDE_2_6+j] = rand();
            M1[i*MAX_MATR_SIDE_2_6+j] = rand();
        }
}
void ref_mult_matrices_2_6() {
    // Zero-initialize the reference and return matrices.
    for (int i = 0; i < MAX_MATR_SIDE_2_6; i++)
        for (int j = 0; j < MAX_MATR_SIDE_2_6; j++) {
            M2[i*MAX_MATR_SIDE_2_6+j] = 0;
            M2_ref[i*MAX_MATR_SIDE_2_6+j] = 0;
        }
    // Compute the reference multiplication.
    for (int i = 0; i < M0_height; ++i)
        for (int j = 0; j < M1_width; ++j) {
            for (int k = 0; k < M1_height; ++k) {
                M2_ref[i*M1_width+j] += M0[i*M0_width+k] * M1[k*M1_width+j];
            }
        }
}

int compare_matrices_2_6() {
    for (int i = 0; i < M0_height; ++i)
        for (int j = 0; j < M1_width; ++j) {
            if (M2_ref[i*M1_width+j] != M2[i*M1_width+j]) {
                printf("Got mismatch at x=%d,y=%d, expected %d, got %d.\n", j, i, M2_ref[i*M1_width+j], M2[i*M1_width+j]);
                return 0;
            }
        }
    return 1;
}

int dummy_solution_2_6() {
    for (int i = 0; i < M0_height; ++i)
        for (int j = 0; j < M1_width; ++j) {
            for (int k = 0; k < M0_width; ++k) {
                M2[i*M1_width+j] += M0[i*M0_width+k] * M1[k*M1_width+j];
            }
        }
}


static inline int test_assignment_2_6(int num_reps) {
    int is_success = 1;
    for (int i = 0; i < num_reps; i++) {
        // Make a trivial multiplication for the first test case.
        if (i == 0) {
            M0_height = 1;
            M0_width = 1;
            M1_width = 1;
        }
        else {
            M0_height = 1+(rand() % (MAX_MATR_SIDE_2_6-1));
            M0_width  = 1+(rand() % (MAX_MATR_SIDE_2_6-1));
            M1_width  = 1+(rand() % (MAX_MATR_SIDE_2_6-1));
        }
        M1_height = M0_width; // Necessary for the matrices to be multipliable.

        // You may uncomment this to help debugging.
        printf("Testing assignment 2_6 with sizes:\n");
        printf("M0_height : %d\n", M0_height);
        printf("M0_width  : %d\n", M0_width);
        printf("M1_width  : %d\n", M1_width);

        populate_matrices_2_6();
        ref_mult_matrices_2_6();
        assignment_2_6(M0, M0_width, M0_height, M1, M1_width, M2);
        if(!compare_matrices_2_6(M2, M2_ref)) {
            printf("assignment_2_6 failed: wrong matrix multiplication result.\n");
            is_success = 0;
            return is_success;
        }
    }
    return is_success;
}


int main(int argc, char **argv) {
    srand(0);

    int num_reps = 100;

    int success = test_assignment_2_6(num_reps) == 1;
    printf("\nsuccess: %d\n", success);
}