// This program will check your assignments (a further check for forbidden instructions happens after this program).
// Feel free to modify this file to help you in debugging your assembly code.
// Note that if you modify this file, there may be mismatches between the grade that you see from your local tests, and the grade that you will receive.

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

extern int64_t assignment_2_4(void (*fun_ptr)(void));

static int is_function_accessed_2_4;
void function_2_4() {
    is_function_accessed_2_4 = 1;
    // Unset some caller-saved registers
    asm volatile ("addi t0, x0, 0");
    asm volatile ("addi t1, x0, 0");
    asm volatile ("addi t2, x0, 0");
    asm volatile ("addi t3, x0, 0");
    asm volatile ("addi t4, x0, 0");
    asm volatile ("addi t5, x0, 0");
    asm volatile ("addi t6, x0, 0");
}

typedef struct {
    int64_t sp; 
    int64_t s[12];
} saved_regs_t;

static inline saved_regs_t save_regs() {
    saved_regs_t ret;
    asm volatile ("mv %0, sp" : "=r" (ret.sp));
    asm volatile ("mv %0, s0" : "=r" (ret.s[0]));
    asm volatile ("mv %0, s1" : "=r" (ret.s[1]));
    asm volatile ("mv %0, s2" : "=r" (ret.s[2]));
    asm volatile ("mv %0, s3" : "=r" (ret.s[3]));
    asm volatile ("mv %0, s4" : "=r" (ret.s[4]));
    asm volatile ("mv %0, s5" : "=r" (ret.s[5]));
    asm volatile ("mv %0, s6" : "=r" (ret.s[6]));
    asm volatile ("mv %0, s7" : "=r" (ret.s[7]));
    asm volatile ("mv %0, s8" : "=r" (ret.s[8]));
    asm volatile ("mv %0, s9" : "=r" (ret.s[9]));
    asm volatile ("mv %0, s10" : "=r" (ret.s[10]));
    asm volatile ("mv %0, s11" : "=r" (ret.s[11]));
    return ret;
}

static inline int check_saved_regs(saved_regs_t saved) {
    int64_t tmp;
    int ret = 0;
    asm volatile ("mv %0, sp" : "=r" (tmp));
    if (tmp != saved.sp) ret |= 1 << 0;
    asm volatile ("mv %0, s0" : "=r" (tmp));
    if (tmp != saved.s[0]) ret |= 1 << 1;
    asm volatile ("mv %0, s1" : "=r" (tmp));
    if (tmp != saved.s[1]) ret |= 1 << 2;
    asm volatile ("mv %0, s2" : "=r" (tmp));
    if (tmp != saved.s[2]) ret |= 1 << 3;
    asm volatile ("mv %0, s3" : "=r" (tmp));
    if (tmp != saved.s[3]) ret |= 1 << 4;
    asm volatile ("mv %0, s4" : "=r" (tmp));
    if (tmp != saved.s[4]) ret |= 1 << 5;
    asm volatile ("mv %0, s5" : "=r" (tmp));
    if (tmp != saved.s[5]) ret |= 1 << 6;
    asm volatile ("mv %0, s6" : "=r" (tmp));
    if (tmp != saved.s[6]) ret |= 1 << 7;
    asm volatile ("mv %0, s7" : "=r" (tmp));
    if (tmp != saved.s[7]) ret |= 1 << 8;
    asm volatile ("mv %0, s8" : "=r" (tmp));
    if (tmp != saved.s[8]) ret |= 1 << 9;
    asm volatile ("mv %0, s9" : "=r" (tmp));
    if (tmp != saved.s[9]) ret |= 1 << 10;
    asm volatile ("mv %0, s10" : "=r" (tmp));
    if (tmp != saved.s[10]) ret |= 1 << 11;
    asm volatile ("mv %0, s11" : "=r" (tmp));
    if (tmp != saved.s[11]) ret |= 1 << 12;
    return ret;
}

static inline int test_assignment_2_4(int num_reps) {
    int is_success = 1;
    is_function_accessed_2_4 = 0;
    for (int i = 0; i < num_reps; i++) {
        saved_regs_t saved = save_regs();
        int saved_regs_check_dummy = check_saved_regs(saved);
        assignment_2_4(function_2_4);
        int saved_regs_check = check_saved_regs(saved);
        if (saved_regs_check ^ saved_regs_check_dummy) {
            printf("assignment_2_4 failed: tampered with a callee-saved register without restoring it.\n");
            return 0;
        }
        if(!is_function_accessed_2_4) {
            printf("assignment_2_4 failed to access function at addr a=0x%p.\n", function_2_4);
            return 0;
        }
    }
    return is_success;
}


int main(int argc, char **argv) {
    srand(0);
    
    int num_reps = 100;

    int success = test_assignment_2_4(num_reps) == 1;
    printf("\nsuccess: %d\n", success);
}