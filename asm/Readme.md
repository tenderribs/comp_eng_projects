# DESCRIPTION

## How to test your implementation

To test all your challenges at once, you can type in the terminal on the server the word make:
```bash
make
```

To test just one challenge, for example the challenge 1_5, type in the terminal on the server:
```bash
make 1_5
```

If you get an error message similar to `make: *** No targets specified and no makefile found.  Stop.`, then you are probably not in the correct directory.
In that case, please use the `cd` command seen in class to get to the correct directory.
If you are in the correct directory, when when you type `ls`, you should see a file named `Makefile`.

You should ignore the warning `Missing #address-cells in interrupt provider`.

## RISC-V assembly in short

Programmers like to type code in high-level languages such as C and C++, but a CPU is unable to understand these languages directly.
_Compilation_ is the action of transforming sources written in a higher-level language into a representation understandable by a CPU: the _bytecode_.

Because bytecode is simply a sequence of bytes, it is very difficult to read for humans. _Assembly language_ is therefore a low-level language, readable by humans, which describes sequences of instructions that map directly to bytecode.

For example, the following snippet of C codes is compiled in the following way:

Source in C or C++ language:
```c
a = a+b
```

Corresponding RISC-V assembly (assuming `a` is stored in register `a5` and `b` is stored in register `a4`):
```asm
addw    a5,a5,a4
```

## Instruction set architectures

Historically, different vendors built CPUs that support various types of instructions.
The set of instructions accepted by a CPU, along with the binary representation of each instruction, is called an _instruction set architecture_ (ISA).

Some ISAs only contain a modest number of instructions (for example RISC-V or MIPS) and are called reduced instruction set computers (RISC). Some others contain a much larger number of instructions, typically more than a thousand. This is the case of the wide-spread x86 ISA.

## RISC-V

RISC-V is a recent free ISA, whereas typical ISA vendors require CPU manufacturers to pay a fee to build CPUs that understand their ISA.

The RISC-V ISA is modular: the base ISA RISCV32I has a very small number of instructions.
This base ISA can then be augmented with various extensions that add instructions supporting features such as floating-point operations, compressed instructions or multiplications and divisions.

# TASKS

During this session, you are asked to implement relatively small snippets of code in RISC-V assembly, in files with the `.s` extension located in the `challenges` directory.

We encourage you to use a RISC-V cheat sheet to have an overview of the RISC-V instructions, for example [this one](https://www.cl.cam.ac.uk/teaching/1516/ECAD+Arch/files/docs/RISCVGreenCardv8-20151013.pdf).

## Forbidden instructions

A comment in each challenge indicates which instructions you are allowed to use. All instructions and pseudo-instructions are forbidden if not explicitly authorized.

## Timeouts

Each challenge must run in less than 5 seconds.
Your challenges will be killed automatically after 5 seconds.

# TIPS

Be careful to preserve the callee-saved registers and to respect the general RISC-V call conventions (for instance, function arguments are provided in registers a0,a1,... and return values are generally provided in register a0) (more information [here](https://riscv.org/wp-content/uploads/2015/01/riscv-calling.pdf)).

In some advanced snippets, be careful to not overwrite the stack written by functions calling the current function.
The stack pointer is always located at the last already-populated double-word.

## Debugging

We provide you part of the code (`tester_src/tester_*.c`) that checks your answers.
You may modify it to help you debugging.
However, you may sometimes want to use the original version of the `tester_src/tester_*.c` files to make sure that your results are correct.

# TESTS

## Schedule

Indicatively, assignments 1.1-1.9 are resolvable after the first assembly lecture.
Assignments 2.0-2.6 are resolvable after the second assembly lecture.

## Debugging

This session is made of 1 example challenge and 16 small challenges of respective grading scale:

| Assignment | Grading scale |
|------------|---------------|
| 1.0        | 0             |
| 1.1        | 5             |
| 1.2        | 5             |
| 1.3        | 5             |
| 1.4        | 5             |
| 1.5        | 5             |
| 1.6        | 5             |
| 1.7        | 5             |
| 1.8        | 7             |
| 1.9        | 7             |
| 2.0        | 7             |
| 2.1        | 7             |
| 2.2        | 7             |
| 2.3        | 7             |
| 2.4        | 7             |
| 2.5        | 7             |
| 2.6        | 9             |
| Total      | 100           |

Each of these challenges is graded as pass or fail.

The challenge 1.0 is to provide you with an example. It is not graded and you are not expected to modify it.
