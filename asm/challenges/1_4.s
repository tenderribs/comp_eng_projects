# Output register:
#   a0

# Task:
#   a0 is a 64-bit integer.
#   Set a0's value to 0xbadcab1e.

# Authorized:
# sll, slli, srl, srli, sra, srai
# add, addi, sub, lui

# Hints:
# - The auipc instruction sets the 20 top bits of the target register.
# - The addi instruction sets the 12 bottom bits of the target register. But problem: it sign-extends it! Can you work around it?

.text
.align	1
.globl	assignment_1_4
.type	assignment_1_4, @function
assignment_1_4:
    # Place first part in mem.
    lui a0, 0xbadca

    # place latter part in mem, move in des. position
    lui t0, 0xb1e
    srli t0, t0, 12

    # combine strings
    add a0, t0, a0

    # get rid of leading ones
    slli a0, a0, 32
    srli a0, a0, 32
    # -- End of assignment code.

   jr ra # Return to the testing framework. Don't modify.
