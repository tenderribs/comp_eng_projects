# Input register:
#   a0
# Output register:
#   a0

# Task:
#   a0 is guaranteed to be in [0, 48]
#   Compute the a0-th term of the Fibonacci sequence, then store it into a0.

# Authorized:
# beq, bne, blt, bge, bltu, bgeu
# add, addi, sub
# jal

.text
.align	1
.globl	assignment_2_1
.type	assignment_2_1, @function
assignment_2_1:

    # Assignment code.

    

    
    # initialise first two fibonacci numbers
    add t0, zero, zero 
    addi t1, zero, 1

    # for a0 == 0, do nothing
    beq a0, zero, finish

    loop:
        beq a0, zero, exit     #if a0 == 0 exit loop
        add t2, t1, zero       #compute new fibonacci numbers
        add t1, t1, t0
        add t0, t2, zero
        addi a0, a0, -1       
    beq zero, zero, loop       #loop

    exit:
    add a0, t0, zero           #store t0 in a0
    
    finish:
    # -- End of assignment code.
    
    jr ra # Return to the testing framework. Don't modify.
