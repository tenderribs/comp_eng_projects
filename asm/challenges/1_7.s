# Input registers:
#   a0, a1, a2
# Output register:
#   a0

# Task:
#   a0 is guaranteed to be 0 or 1.
#   If a0 == 0, then set a0 to the value of a1. Else, set a0 to the value of a2.
#   No jumps or branches are allowed except for the return.

# Authorized:
# sll, slli, srl, srli, sra, srai
# xor, xori, or, ori, and, andi

# Hint:
# - None of the authorized instructions permits to change the control flow (e.g., bne and jal are not authorized).

.text
.align	1
.globl	assignment_1_7
.type	assignment_1_7, @function
assignment_1_7:
    # operation: a0 <-- a1*(1 - a0) + a2*a0

    # initialize registers. want 11..111 if a0 == 1, 000..00 if a0 == 0
    slli a0, a0, 63
    srai a0, a0, 63

    # t1 <-- a2*a0
    and t1, a0, a2

    # flip bits
    xori a0, a0, -1

    # t2 <-- a1*(1 - a0)
    and t2, a0, a1

    # perform addition
    or a0, t1, t2
    # -- End of assignment code.

    jr ra # Return to the testing framework. Don't modify.
