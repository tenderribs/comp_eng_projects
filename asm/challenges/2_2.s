# Input register:
#   a0
# Output register:
#   a0

# Task:
#   a0 is the base address of a 32-bit (4-byte) integer table in memory.
#   Your task is to find the number of nonzero elements before the first zero element in the table.
#   It is guaranteed that the table contains at least one zero element.

# Authorized:
# jal
# lb, lh, lw, ld, lbu, lhu, lwu, ldu
# sb, sh, sw, sd
# add, addi, sub
# beq, bne, blt, bge, bltu, bgeu

.text
.align	1
.globl	assignment_2_2
.type	assignment_2_2, @function
assignment_2_2:
    # Assignment code.
    addi t0, zero, 0            # count = 0

    while_not_found:
        lw t1, 0(a0)            # load value
        beq zero, t1, found

        addi t0, t0, 1          # count++
        addi a0, a0, 4
        beq zero, zero, while_not_found

    found:
    add a0, zero, t0            # set return value

    # -- End of assignment code.

    jr ra # Return to the testing framework. Don't modify.
