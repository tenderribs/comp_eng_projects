# Input registers:
#   a0, a1, a2, a3, a4, a5
# Output register: None

# Task:
#   Your task is to multiply two matrices M0 and M1 of 16-bit (2-byte) signed integers and to store the resulting matrix M2 in memory.
#   a0 contains the base address of M0.
#   a1 contains the width of M0  (number of columns). Included in [1, 30]. It is the same as the height of M1.
#   a2 contains the height of M0 (number of rows). Included in [1, 30]. It is the same as the height of M2.
#   a3 contains the base address of M1.
#   a4 contains the width of M1  (number of columns). Included in [1, 30]. It is the same as the width of M2.
#   a5 contains the (pre-allocated) address where to store M2.

# Authorized:
# sll, slli, srl, srli, sra, srai
# lb, lh, lw, ld, lbu, lhu, lwu, ldu
# sb, sh, sw, sd
# add, addi
# beq, bne, blt, bge, bltu, bgeu
# jal, jalr
# sll, slli, srl, srli, sra, srai
# mul

# Hint:
# - Feel free to modify the file tester_main.c, for example to add prints. This may help you a lot debugging your code.

.text
.align	1
.globl	assignment_2_6
.type	assignment_2_6, @function
assignment_2_6:
    # Assignment code.

    # reference symbolic multiplication:
    # for (int i = 0; i < M0_height; ++i) {
    #     for (int j = 0; j < M1_width; ++j) {
    #         for (int k = 0; k < M1_height; ++k) {
    #             M2_ref[i * M1_width + j] += M0[i * M0_width + k] * M1[k * M1_width + j];
    #         }
    #     }
    # }

    # vars replaced with registers:
    # for (int t0 = 0; t0 < a2; ++t0) {
    #     for (int t1 = 0; t1 < a4; ++t1) {
    #         for (int t2 = 0; t2 < a1; ++t2) {
    #             a5[t0 * a4 + t1] += a0[t0 * a1 + t2] * a3[t2 * a4 + t1];
    #         }
    #     }
    # }

    add t0, zero, zero
    for_t0:
        bgeu t0, a2, exit

        add t1, zero, zero
        for_t1:
            bgeu t1, a4, end_for_t0

            add t2, zero, zero
            for_t2:
                bgeu t2, a1, end_for_t1

                # BEGIN: calculation

                # M0 offset t4 <- t0 * a1 + t2
                mul t4, t0, a1
                add t4, t4, t2

                # M1 offset t5 <- t2 * a4 + t1
                mul t5, t2, a4
                add t5, t5, t1

                # M2 offset t6 <- t0 * a4 + t1
                mul t6, t0, a4
                add t6, t6, t1

                # apply offset to base addresses
                slli t4, t4, 1      # a0
                add t4, a0, t4
                slli t5, t5, 1      # a3
                add t5, a3, t5
                slli t6, t6, 1      # a5
                add t6, a5, t6

                # load values from DRAM
                lh t4, 0(t4)
                lh t5, 0(t5)
                lh t3, 0(t6)

                # multiply and sum loaded values
                mul t4, t4, t5
                add t3, t3, t4

                # save result back into DRAM
                sh t3, 0(t6)

                # END: calculation

                addi t2, t2, 1
                beq zero, zero, for_t2

            end_for_t1:
                addi t1, t1, 1
                beq zero, zero, for_t1

        end_for_t0:
            addi t0, t0, 1
            beq zero, zero, for_t0

    exit:

    # -- End of assignment code.

    jr ra # Return to the testing framework. Don't modify.
