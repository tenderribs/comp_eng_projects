# Input register:
#   a0,a1
# Output register:
#   a0

# Task:
#   a0 is the base address of a 32-bit (4-byte) signed integer (tip: we work with 64-bit wide registers).
#   a1 indicates whether this integer has been stored in little-endian (a1 = 0) or big-endian (a1 = 1) mode.
#   Your task is to retrieve the integer from memory and to return it.

# Authorized:
# jal
# lb, lh, lw, ld, lbu, lhu, lwu, ldu
# add, addi, sub, or
# sll, slli, srl, srli, sra, srai
# beq, bne

.text
.align	1
.globl	assignment_2_3
.type	assignment_2_3, @function
assignment_2_3:

    # Assignment code.
    beq a1, zero, little    # check endianness, if little then nothing to do

    big:
        # load bytes one-by one and shift accordingly
        lbu t0, 0(a0)
        slli t0, t0, 24

        lbu t1, 1(a0)
        slli t1, t1, 16
        add t0, t0, t1

        lbu t1, 2(a0)
        slli t1, t1, 8
        add t0, t0, t1

        lbu t1, 3(a0)
        add t0, t0, t1

        beq zero, zero, exit

    little:
        # little endian is default, so load word directly
        lw t0, 0(a0)

    exit:
        add a0, zero, t0

    # -- End of assignment code.

    jr ra # Return to the testing framework. Don't modify.
