# Input registers:
#   a0, a1
# Output register:
#   a0

# Task:
#   a0 is a 64-bit integer.
#   a1 is guaranteed to be included in [1, 64].
#   Your task is to zero out the a1 top bits of a0.
#   Example: if a0 = 0b1101000110100010011100111000110111010001101000100111001110001101 and a1 = 7, then a0 must be set to 0b0000000110100010011100111000110111010001101000100111001110001101.

# Hint:
#  - The immediate of slli, srli and srai is encoded on 5 bits only.

# Authorized:
# sll, slli, srl, srli, sra, srai
# add, addi, sub
# xor, xori, or, ori, and, andi

.text
.align	1
.globl	assignment_1_6
.type	assignment_1_6, @function
assignment_1_6:

    # create bitmask ~ 0000000011111111
    addi t1, zero, -1
    srl t1, t1, a1

    and a0, t1, a0
    # -- End of assignment code.

    jr ra # Return to the testing framework. Don't modify.
