# Input register:
#   a0
# Output register:
#   a0

# Task:
#   a0 is a 64-bit integer.
#   Invert the a0 value bitwise (each zero bit of a0 must be set to 1 and vice-versa).

# Authorized:
# add, addi, sub
# xor, xori
# slt

.text
.align	1
.globl	assignment_1_3
.type	assignment_1_3, @function
assignment_1_3:

    # Assignment code.
    xori a0, a0, -1
    
    # -- End of assignment code.

    jr ra # Return to the testing framework. Don't modify.
