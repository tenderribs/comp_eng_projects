# Input register:
#   a0
# Output register:
#   a0

# Task:
#   a0 is the entry point of a function provided to you.
#   Your task is to transfer control to this function.
#   Don't forget to provide the return address in x1 (also known as ra) & to save it beforehand.

# Authorized:
# jal, jalr
# lb, lh, lw, ld, lbu, lhu, lwu, ldu
# sb, sh, sw, sd
# add, addi

.text
.align	1
.globl	assignment_2_4
.type	assignment_2_4, @function
assignment_2_4:
    # Assignment code.
    addi sp, sp, -16     # save in stack
    sw ra, 0(sp)

    jalr ra, 0(a0)      # jump to function

    lw ra, 0(sp)        # load ra back
    addi sp, sp, 16
    # -- End of assignment code.

    jr ra # Return to the testing framework. Don't modify.
