# Input register:
#   a0
# Output register:
#   a0

# Task:
#   a0 is a 64-bit integer. It is guaranteed to be included in [1, 64].
#   Your task is to create a binary number with a0 ones on the most significant bit side, and the potential other bits are zero.
#   Example: if a0 = 9, then a0 must be set to 0b1111111110000000000000000000000000000000000000000000000000000000, where `0b` means `written in binary and not in decimal or hexadecimal`.

# Hint:
#  - The immediate of slli, srli and srai is encoded on 5 bits only.

# Authorized:
# sll, slli, srl, srli, sra, srai
# add, addi, sub

.text
.align	1
.globl	assignment_1_5
.type	assignment_1_5, @function
assignment_1_5:
    # place a 1 at leftmost spot
    addi t1, zero, 1
    slli t1, t1, 63

    # manually already placed a one, so a0--
    addi a0, a0, -1

    # pull 1 over to the right, creating 111110...00
    sra a0, t1, a0
    # -- End of assignment code.

    jr ra # Return to the testing framework. Don't modify.
