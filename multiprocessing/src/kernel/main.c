#include "util.h"
#include "buddy.h"
#include "pt.h"
#include "process.h"
#include "test.h"
#include "exception.h"

void main()
{
	printstr(YAK_LOGO);

	buddy_init();

#ifdef TEST
	buddy_test();
#endif

	pt_init();
	pt_jump_to_high();
	pt_destroy_low_kvmas();

#ifdef TEST
	printstr("Starting the tests for pt.c...\n\n");
	pt_test();
#endif

	proc_init_process_list();

	set_sscratch();

	/*
	 * TODO: When you are ready, enable the timer interrupt by uncommenting
	 * the following line
	 */
	ecall_timer_setup();

	// FIXME: why doesnt it work if it's used
	// directly?
	void *ptr = &_testing;
	printstr("Starting testing process...: \n");
	int ret = proc_run_process(ptr, 0, NORMAL_PAGES);

	if (ret) {
		printdbg("ERROR: Could not test process: \n",
			 (void *)(unsigned long)ret);
	}
}
