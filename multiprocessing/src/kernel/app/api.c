#include "api.h"

/*mmap
 *Performs a SCALL_MMAP
 *FIXME: Maybe add parameters for page size?
 *@pagen: number of pages requested
 */
uint64_t mmap(const char pagen)
{
	return syscall(SCALL_MMAP, pagen, 0, 0);
}

/*put
 *Performs a SCALL_PRINT
 *@a: character to print 
 */
void put(const char a)
{
	syscall(SCALL_PRINT, a, 0, 0);
}

/*getpid
 *return: current process PID
 */
int getpid(void)
{
	return (int) syscall(SCALL_GETPID, 0, 0, 0);
}

/*yield
 *Yields the current process
 */
void yield(void)
{
	syscall(SCALL_YIELD, 0, 0, 0);
}

/*end
 *Ends the current process
 */
void end(void)
{
	syscall(SCALL_END, 0, 0, 0);
}

/*evoke
 *Performs a SCALL_EXECV
 *@arg1: a program to execute (picked from PROGRAMS enum)
 *@arg2: arguments for the main function (fetched from the application by get_arg())
 *@arg3: specifies if the process is to be mapped with HUGE PAGES or not. (see PROC_EXTRA_ARGS enum)
 */
int evoke(const char arg1, const char arg2, const char arg3)
{
	return (int) syscall(SCALL_EXECV, arg1, arg2, arg3);
}

/*syscall
 * Performs a syscall
 * @ID: syscall type (see ../scalls.h)
 * @arg1-3: arguments specific for the syscall
 */
uint64_t syscall(const char ID, const char arg1, const char arg2, const char arg3)
{
	uint64_t retval;

	asm volatile("mv a0, %[ID]\n\t"
		     "mv a1, %[arg1]\n\t"
		     "mv a2, %[arg2]\n\t"
		     "mv a3, %[arg3]\n\t"
		     "ecall\n\t"
		     "mv %[retval], a0\n\t"
		     : [retval] "=r"(retval)
		     : [ID] "r"(ID), [arg1] "r"(arg1), [arg2] "r"(arg2), [arg3] "r"(arg3)
		     : "a0", "a1", "a2", "a3", "memory");

	return retval;
}