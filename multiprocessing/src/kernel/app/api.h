#ifndef API_H
#define API_H

#include "../scalls.h"
#include "../types.h"

enum PROGRAMS { TESTING, HELLO_JR };

// Extra arguments for the process evoke
enum PROC_EXTRA_ARGS { NORMAL_PAGES, HUGE_PAGES };

void put(const char a);
int evoke(const char arg1, const char arg2, const char arg3);
void yield(void);
void end(void);
uint64_t mmap(const char arg1);
int getpid(void);
uint64_t syscall(const char ID, const char arg1, const char arg2, const char arg3);
#endif
