#include "util.h"
#include "buddy.h"
#include "pt.h"
#include "process.h"
#include "test.h"
#include "exception.h"

void main()
{
	buddy_init();

#ifdef TEST
	buddy_test();
#endif

	pt_init();
	pt_jump_to_high();
	pt_destroy_low_kvmas();

#ifdef TEST
	printstr("Starting the tests for pt.c...\n\n");
	pt_test();
#endif

	printstr("done!\n");

	/* FIXME: should shutdown */
	for (;;) {
	}
}
