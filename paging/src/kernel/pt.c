#include "pt.h"

/* FIXME: use non-shift version everywhere, i.e., also for gets */
/* EINVALARGS is a bit of a catchall */
#define ENOTMAPPED 1
#define EMAPPED 2

#define VPN_MASK 0x1FF
#define VPN_MASK_WIDTH 9
#define VPN_SHIFT(level) (VPN_MASK_WIDTH * (level))

/* Extracts the nine page table index bits from @vpn for level @level */
#define vpn2vpnlevel(vpn, level) (((vpn) >> VPN_SHIFT(level)) & VPN_MASK)
#define vpn_align(vpn, level) ((vpn) & (-1L << VPN_SHIFT(level)))

/* Some bit magic */
#define __floor2(b, x) ((x) & -(b))
#define __ceil2(b, x) (((x) + (b) + -1) & -(b))

/*
 * What are "pagen", "level", "depth", and "size"? (And "order", but that one
 * should be familiar.)
 *
 * size: simple, size of something in terms of bytes
 *
 * depth: page table depth (from top to bottom), ranges from 0 up to and
 * including 4, where 0 refers to satp, 1 is the depth of the top-level page
 * table (i.e., the table pointed to by satp), etc. Used to walk page tables
 *
 * level: almost the inverse of depth (see enum levels and level2depth): a page
 * of level PAGE_NORMAL (=0) is a page at depth 4 in the page table, and has
 * size 4 KiB. A page of level PAGE_MEGA (=1) is a page at depth 3 in the page
 * table, and has size 2 MiB, etc.
 *
 * pagen: the size, typically of a range of pages, in terms of number of
 * PAGE_NORMAL pages, in other words: 1 means 4 KiB, 2 means 8 KiB, etc.
 *
 * order: from buddy! As you can see, there's a level2order macro. Do you get
 * why it's so simple and order is just level times 9?
 */
#define level2pagen(level) (1UL << (VPN_MASK_WIDTH * (level)))
#define level2order(level) ((level)*9)
#define pagen2ceillevel(pagen)                                                 \
	(ctz(__ceil2(1L << VPN_MASK_WIDTH, (pagen))) / VPN_MASK_WIDTH)
#define pagen2floorlevel(pagen)                                                \
	(ctz(__floor2(1L << VPN_MASK_WIDTH, (pagen))) / VPN_MASK_WIDTH)
#define level2size(level) (level2pagen(level) * PAGE_SIZE)

#define vpn2prevpageboundary(vpn, level) ((vpn) & (-1L * level2pagen(level)))
#define vpn2nextpageboundary(vpn, level)                                       \
	(((vpn) + level2pagen(level) - 1) & (-1L * level2pagen(level)))

extern struct buddy_layout layout;

/* Kernel satp: the kernel's address space */
unsigned long ksatp;

#ifdef PT_DEBUG
/*
 * Functions for debugging (see end of this file)
 */
static void pt_pte_print(unsigned long pte);
#endif

/*
 * Pointer to the head node of a linked list of kernel VMAs. Unlike the
 * non-head nodes, the head node is not part of a VMA structure! This means
 * that calling member_to_struct on the head node will give you trouble (don't
 * worry if you don't understand this the first time you read it, it's not
 * easy).
 *
 * FIXME: should be moved inside a task struct later
 */
struct node *kvmas_head;
struct vma *kvmas;

/* 4 KiB, 2 MiB, 1 GiB, 512 GiB */
enum levels { PAGE_NORMAL, PAGE_MEGA, PAGE_GIGA, PAGE_TERA };

static void *(*phys2virt)(unsigned long);

/*
 * For after the MMU has been turned on
 */
static void *phys2highkvirt(unsigned long phys)
{
	return (void *)(VIRT_MEM_KERNEL_BASE + phys);
}

/*
 * For until the MMU has been turned on
 *
 * The "phys" that is returns behaves like a virtual address, in the sense that
 * it's a valid address that can be accessed (we typically cannot access
 * physical addresses directly)
 */
static void *phys2phys(unsigned long phys)
{
	return (void *)phys;
}

/*
 * See pt.h
 */
unsigned long pt_alloc(unsigned long level)
{
	struct block *block = buddy_alloc(level2order(level));

	if (!block) return 0;

	memset(phys2virt(ppn2phys(block2ppn(block))), 0,
	       PAGE_SIZE * (1UL << level2order(level)));

	return ppn2phys(block2ppn(block));
}

/*
 * See pt.h
 */
int pt_free(unsigned long phys)
{
	return buddy_free(ppn2block(phys2ppn(phys)));
}

/*
 * See pt.h
 */
void *pt_salloc(unsigned long size)
{
	static unsigned long base = 0;
	static unsigned long cursor = 0;

	if (!size) return NULL;

	/*
	 * For allocations larger than size PAGE_NORMAL, use another allocator
	 */
	if (size > level2size(PAGE_NORMAL)) return NULL;

	if (!base) {
		base = pt_alloc(PAGE_NORMAL);
		if (!base) return NULL;
	}

	if (cursor + size > level2size(PAGE_NORMAL)) {
		base = 0;
		cursor = 0;
		return pt_salloc(size);
	}

	void *ptr = phys2virt(base + cursor);
	cursor += size;

	return ptr;
}

/*
 * pt_next_pte: one step in a page table walk
 *
 * @depth: index into @ptes, starting point of the step
 * @vpn: virtual page number, we need it to determine where to go next
 * @ptes: list of PTEs encountered "along the way"
 *
 * Returns a pointer to the most recent PTE in @ptes, i.e., the result of our
 * step
 *
 */
static unsigned long *pt_next_pte(unsigned long depth, unsigned long vpn,
				  unsigned long *ptes)
{
	/*
	* TODO: make a step in the page table walk. This is difficult. Hint:
	* first construct a physical pointer to the next PTE, then convert
	* that pointer to a virtual address, and finally, access it
	* (dereference it) and store it at depth + 1 in @ptes. Return the
	* pointer that you dereferenced in the final step.
	*
	* You will need the following macros: ppn2phys, pte_get_ppn,
	* vpn2vpnlevel, depth2level, and phys2virt
	*
	* The good news: just four lines of code: one addition, a call to
	* phys2virt, one dereference (don't forget to update @ptes), and a
	* return
	*/

	/**
	 * first discard 12 offset bits. vpn2vpnlevel extracts 9 bits of the vpn[level]. For instance
	 * vpn2vpnlevel(vpn >> 12, 3) tries to extract the leftmost 9 bits of vpn.
	 *
	 * This means we need to shift the vpn by only 27 bits to the right (not 36! otherwise we shift past all data in vpn).
	 * For this, vpn2vpnlevel needs to see level - 1, not just level. Level is 1 higher that it needs to be.
	*/
	unsigned long next_page_table_base_ppn = pte_get_ppn(ptes[depth]);

	unsigned long vpn_at_level = vpn2vpnlevel(vpn, depth2level(depth + 1));

	/**
	 * To the page table base address, we add the offset to find the pointer to the next table.
	 * The number 8 comes from the fact that every PTE is 8 bytes large (4KiB / 512 = 8).
	*/
	unsigned long phys_next_pte =
		ppn2phys(next_page_table_base_ppn) + vpn_at_level * 8;

	unsigned long *pte_next = phys2virt(phys_next_pte);
	ptes[depth + 1] = *pte_next;

	return pte_next;
}

/*
 * pt_get_pte: gets the PTE corresponding to @vpn
 *
 * @satp: address space to search in
 * @vpn: virtual page number to look for
 * @ptes: array of PTEs that will be filled along the way, i.e. page walk
 *
 * @out: will make the pointer you give it point to the PTE it found (which can
 * be used to then change it)
 *
 * Returns the depth at which the PTE was found, can be used to index
 * into @ptes
 *
 */
static int pt_get_pte(unsigned long *satp, unsigned long vpn,
		      unsigned long *ptes, unsigned long **out)
{
	unsigned long *curr_pte = satp;
	ptes[0] = satp2pte(*satp);

	for (unsigned depth = 0; depth < PT_NUM_LEVELS; depth++) {
		if (pte_is_leaf(ptes[depth])) {
			if (out) *out = curr_pte;
			return depth;
		} else if (!pte_is_valid(ptes[depth])) {
			return -ENOTMAPPED;
		}

		if (depth < PT_NUM_LEVELS - 1) {
			curr_pte = pt_next_pte(depth, vpn, ptes);
		}
	}

	assert(0);
}

/*
 * pt_alloc_pt: page table allocator
 *
 * Returns a page table entry that points to a newly allocated page table
 */
unsigned long pt_alloc_pt(void)
{
	unsigned long pte = 0;
	unsigned long ppn = phys2ppn(pt_alloc(0));

	if (!ppn) return 0;

	pte = pte_set_ppn(pte, ppn);
	pte = pte_set(pte, PTE_VALID_SHIFT);

	return pte;
}

/*
 * pt_ste_pte_nonleaf: sets a nonleaf page table entry
 *
 * @pte_ptr: a pointer to the page table entry that should be set
 *
 * Returns 0 on success and -error otherwise
 */
static void pt_set_pte_nonleaf(unsigned long *pte_ptr)
{
	unsigned long pte = pt_alloc_pt();

	assert(pte);

	*pte_ptr = pte;
	pt_flush_tlb();
}

/*
 * pt_set_pte_leaf: page table walker
 *
 * @satp: pointer to the page table root
 * @vpn: virtual page number, need it to find out where to go in our page table
 * @pte: the new page table entry, will replace @ptes[depth] in the page table
 * @depth: depth of entry into @ptes that the caller wishes to update
 *
 * Returns 0 on success and -EMAPPED if there's already something mapped @vpn
 *
 */
static int pt_set_pte_leaf(unsigned long *satp, unsigned long vpn,
			   unsigned long pte, unsigned long depth)
{
	unsigned long ptes[PT_NUM_LEVELS] = { 0 };

	/* NOTE: pointer to curr. PTE, not to where curr. PTE points to! */
	unsigned long *curr_pte = satp;
	ptes[0] = satp2pte(*satp);
	// printdbg("ptes[0]:", ptes[0]);

	/* Let's walk the page table! */
	for (unsigned long j = 0; j < depth; j++) {
		/*
		 * TODO: implement the walk, think of the following cases:
		 *
		 * 1. ptes[j] is a leaf. This is a problem, why? Use assert!
		 *
		 * 2. ptes[j] is invalid. This is not a problem, but requires
		 * us to do some extra work before we can move on. Hint: use
		 * pt_set_pte_nonleaf()
		 *
		 * 3. ptes[j] looks good, that is, it's a valid nonleaf PTE.
		 * Time to take a step! Use pt_next_pte here
		 */

		assert(!pte_is_leaf(ptes[j]));
		// assert(j != 1);

		if (!pte_is_valid(ptes[j])) {
			// create a nonleaf page table entry if no valid entry found
			pt_set_pte_nonleaf(curr_pte);

			ptes[j] = *curr_pte;
		}

		// take step by setting the next entry in ptes
		curr_pte = pt_next_pte(j, vpn, ptes);
	}

	/*
	 * PTE already mapped and we're trying to overwrite it with something
	 * else, but note the `&& pte` to make unmapping possible ;)
	 */
	if (pte_is_valid(ptes[depth]) && pte) return -EMAPPED;

	*curr_pte = pte;
	pt_flush_tlb();
	ptes[depth] = *curr_pte;
	return 0;
}

/*
 * pt_map_page: calls pt_set_pte_leaf to create a mapping @vpn
 *
 * This function prepares the PTE that is required by pt_set_pte_leaf: it
 * converts the VMA_* flags into PTE_* bits, sets the PPN, and marks the PTE as
 * valid
 *
 * If @use_ppn is set, it will not call pt_alloc for a new PPN, but instead use
 * @optional_ppn
 */
void pt_map_page(unsigned long *satp, unsigned long vpn, unsigned long flags,
		 unsigned long level, unsigned long optional_ppn,
		 unsigned use_ppn)
{
	unsigned long pte = 0;
	unsigned long ppn = 0;

	assert(level <= PAGE_TERA);

	/*
	 * TODO: call pt_set_pte_leaf here, prepare the PTE based on the
	 * arguments given to this function. Don't forget to mark the PTE as
	 * valid. The PTE will also need a PPN. Use pt_alloc, but only if
	 * @use_ppn is not set. Assert that you get a physical address from
	 * pt_alloc (and not 0x0...)
	 */
	pte = pte_set(pte, PTE_VALID_SHIFT);
	assert(pte_is_valid(pte));
	if (use_ppn) {
		ppn = optional_ppn;
	} else {
		unsigned long phys_addr = pt_alloc(level);
		assert(phys_addr);
		ppn = phys2ppn(phys_addr);
	}
	// pte |= flags;
	pte = pte_set_ppn(pte, ppn);

	// prepare all the flags
	if (flags & VMA_READ) pte = pte_set(pte, PTE_READ_SHIFT);
	if (flags & VMA_WRITE) pte = pte_set(pte, PTE_WRITE_SHIFT);
	if (flags & VMA_EXEC) pte = pte_set(pte, PTE_EXEC_SHIFT);
	if (flags & VMA_USER) pte = pte_set(pte, PTE_USER_SHIFT);

	// set leaf
	pt_set_pte_leaf(satp, vpn, pte, level2depth(level));
}

unsigned pt_highest_possible_level_for_vpn(unsigned long *vpn,
					   unsigned long start,
					   unsigned long pagen)
{
	unsigned level = PAGE_TERA;
	unsigned long tmp = *vpn;

	assert(tmp == vpn2prevpageboundary(tmp, 0));

	/*
	 * TODO: figure out, for the range start + pagen, what the page level
	 * is of the page inside of which *vpn is. (You may want to read that
	 * sentence again.)
	 *
	 * In some cases, you may have to decrement *vpn! In other words, this
	 * function returns two arguments: it updates the VPN via the first
	 * argument, the pointer to @vpn, and returns the level of the page
	 * inside of which VPN should be.
	 *
	 * Consider the following:
	 *
	 * 1. Try to find the biggest level
	 * 2. Naturally, the page should fit inside start + pagen. This means
	 * that it should not go below start, or over start + pagen
	 * 3. VPN should be inside the page
	 *
	 * Make sure to not do "level--" when level is already zero. Use
	 * assertions to prevent this!
	 *
	 * Finally, don't forget to update the VPN if you found that it should
	 * be inside a big page that start below the current VPN.
	 *
	 * Macros to use here: vpn2prevpageboundary and level2pagen!
	 */

	// try the levels > NORMAL
	for (; level > 0; level--) {
		assert(level <= PAGE_TERA);

		// check if level exceeds range
		if ((vpn2prevpageboundary(tmp, level) >= start) &&
		    (vpn2prevpageboundary(tmp, level) + level2pagen(level) <=
		     start + pagen)) {
			// make sure the VPN is aligned to a table boundary
			*vpn = vpn2prevpageboundary(*vpn, level);

			assert(*vpn == vpn2prevpageboundary(*vpn, level));
			return level;
		}
	}

	// make the VPN is aligned to a page boundary
	assert(*vpn == vpn2prevpageboundary(*vpn, PAGE_NORMAL));
	return PAGE_NORMAL;
}

/*
 * pt_unmap_page: sets the PTE corresponding to the page at @vpn to zero
 *
 * Returns the size, in pagen, of the unmapped page, or -error on failure
 */
static long pt_unmap_page(unsigned long *satp, unsigned long vpn)
{
	unsigned long ptes[PT_NUM_LEVELS] = { 0 };
	unsigned long *out = NULL;
	int depth = pt_get_pte(satp, vpn, ptes, &out);

	if (depth < 0) {
		return depth;
	} else {
		*out = 0;
		return level2pagen(depth2level(depth));
	}
}

/*
 * pt_map_page_range: maps the range first_vpn + total_pagen
 *
 * If @use_first_ppn is nonzero, it will use @optional_first_ppn to back the
 * mapping, otherwise it asks memory from buddy. If @use_first_ppn is set, the
 * VPN and PPN should increase linearly, by the same increment.
 *
 * Tries to use the highest level possible if flags & VMA_HUGEPAGES
 */
static void pt_map_page_range(unsigned long *satp, unsigned long first_vpn,
			      unsigned long total_pagen, unsigned long flags,
			      unsigned long optional_first_ppn,
			      unsigned use_first_ppn)
{
	unsigned long pagen = 0;
	unsigned long ppn = use_first_ppn ? optional_first_ppn : 0;

	assert(total_pagen);

	for (unsigned long vpn = first_vpn; vpn < first_vpn + total_pagen;
	     vpn += pagen) {
		/*
		 * TODO: if VMA_HUGEPAGES is set, use
		 * pt_highest_possible_level_for_vpn to figure out the highest
		 * possible level for this VPN's page.
		 *
		 * (Note that pt_highest_possible_level_for_vpn may also update
		 * the VPN. In this function, however, it will never actually
		 * update the VPN, but in the page fault handler, see
		 * exception.c, where something similar happens, it may
		 * sometimes decrease the VPN. Do you see why? If not, you will
		 * probably not pass the access_demand_nomega_overlap test.)
		 *
		 * If VMA_HUGEPAGES is not set, we always use level 0.
		 *
		 * Finally, use pt_map_page to map the page, update pagen and
		 * optionally the ppn, depending on whether use_first_ppn is
		 * set.
		 */
		// assert vpn is aligned with page
		assert(vpn == vpn2prevpageboundary(vpn, 0));

		// determine which level to use
		unsigned level = 0;

		if (flags & VMA_HUGEPAGES) {
			level = pt_highest_possible_level_for_vpn(
				&vpn, first_vpn, total_pagen);
		}

		// map the page
		pt_map_page(satp, vpn, flags, level, ppn, use_first_ppn);
		pagen = level2pagen(level);

		/* If @use_first_ppn is set the VPN and PPN should increase linearly, by the same increment.*/
		// VPN is increased in the for loop
		if (use_first_ppn) {
			ppn += pagen;
		}
	}
}

static void pt_unmap_page_range(unsigned long *satp, unsigned long first_vpn,
				unsigned long total_pagen)
{
	unsigned long pagen = 0;

	for (unsigned long vpn = first_vpn; vpn < first_vpn + total_pagen;
	     vpn += pagen) {
		pagen = pt_unmap_page(satp, vpn);
		assert(pagen);
	}
}

/*
 * See pt_vma_new
 */
void pt_vma_new(unsigned long *satp, unsigned long vpn, unsigned long pagen,
		unsigned long flags, struct vma *out)
{
	/* We assume that @out is already on a linked list somewhere */
	assert(is_node_init(&out->node));

	/* TODO: if VMA_POPULATE is set, then... do something here! */
	if (flags & VMA_POPULATE) {
		pt_map_page_range(satp, vpn, pagen, flags, 0, 0);
	}

	out->vpn = vpn;
	out->pagen = pagen;
	out->flags = flags;
}

/*
 * See pt.h
 */
void pt_vma_destroy(unsigned long *satp, struct vma *vma)
{
	/* FIXME: free @vma (requires slab allocator) */
	list_remove(&vma->node);
	pt_unmap_page_range(satp, vma->vpn, vma->pagen);
}

/*
 * ksatp_init: initialized the "kernel satp" (i.e., the root of the kernel's
 * address space/page tables)
 *
 * Does not yet turn on the MMU!
 */
static void ksatp_init(void)
{
	unsigned long pte = pt_alloc_pt();

	assert(pte);

	/*
	 * Set the MODE field to "Bare": "No translation or protection" (not
	 * yet)
	 */
	ksatp = satp_set(ksatp, SATP_MODE_BARE, SATP_MODE_MASK,
			 SATP_MODE_SHIFT);

	/*
	 * Set the PPN field to a newly allocated ppn, the ppn that we will be
	 * using to construct our kernel page tables
	 */
	ksatp = satp_set(ksatp, pte_get_ppn(pte), SATP_PPN_MASK,
			 SATP_PPN_SHIFT);
}

/*
 * ksatp_enable_paging: turns on the MMU (good luck)
 */
static void ksatp_enable_paging(void)
{
	ksatp = satp_set(ksatp, SATP_MODE_SV48, SATP_MODE_MASK,
			 SATP_MODE_SHIFT);
	csrw(satp, ksatp);
	pt_flush_tlb();
}

/*
 * pt_init_vmas_head: initializes the head of the linked list of kernel/user
 * VMAs, also see kvmas_head declaration above
 */
void pt_init_vmas_head(struct node **vmas_head)
{
	*vmas_head = pt_salloc(sizeof(struct node));

	assert(*vmas_head);

	node_init(*vmas_head);
}

/*
 * pt_alloc_vma: allocates a new kernel/user VMA structure and adds it to the
 * linked list of kernel/user VMAs, but doesn't initialize it otherwise (that's
 * done by pt_vma_new)
 */
struct vma *pt_alloc_vma(struct node *vmas_head)
{
	struct vma *kvma = pt_salloc(sizeof(struct vma));

	if (!kvma) return NULL;

	node_init(&kvma->node);

	if (!is_node_init(vmas_head)) return NULL;

	list_append(vmas_head, &kvma->node);

	return kvma;
}

static void map_lowhigh(unsigned long *satp, unsigned long vpn, long ppn,
			unsigned long pagen, unsigned long flags)
{
	assert(pagen);
	assert(!(flags & VMA_POPULATE));
	assert(flags & VMA_HUGEPAGES);

	struct vma *low = pt_alloc_vma(kvmas_head);

	assert(low);

	struct vma *high = pt_alloc_vma(kvmas_head);

	assert(high);

	/*
	 * We will remove the low mapping and VMA soon, once we jump high. We
	 * could also not create a VMA here but then we have to "manually"
	 * call unmap_page_range for the right ranges, this is cleaner
	 */
	pt_map_page_range(&ksatp, ppn, pagen, flags, ppn, 1);
	pt_vma_new(&ksatp, ppn, pagen, flags, low);

	pt_map_page_range(&ksatp, vpn, pagen, flags, ppn, 1);
	pt_vma_new(&ksatp, vpn, pagen, flags, high);
}

static void map_phys_mem_lowhigh(void)
{
	/* FIXME: PTE_GLOBAL_SHIFT? */
	unsigned long flags = VMA_READ | VMA_WRITE | VMA_HUGEPAGES;
	unsigned long pagen = phys2ppn(layout.phys_end - layout.phys_base);
	unsigned long ppn = phys2ppn(layout.phys_base);
	unsigned long vpn = virt2vpn(VIRT_MEM_KERNEL_BASE + ppn2phys(ppn));

	map_lowhigh(&ksatp, vpn, ppn, pagen, flags);
}

/*
 * FIXME: special case .data and .bss, we don't want them to be executable or
 * writable
 *
 * Includes meta data for buddy
 */
static void map_kelf_lowhigh(void)
{
	/* FIXME: PTE_GLOBAL_SHIFT? PTE_ACCESSED_SHIFT? */
	unsigned long flags = VMA_READ | VMA_WRITE | VMA_EXEC | VMA_HUGEPAGES;
	unsigned long pagen = phys2ppn(layout.phys_base - layout.kelf_base);
	unsigned long ppn = phys2ppn(layout.kelf_base);
	unsigned long vpn = virt2vpn(VIRT_MEM_KERNEL_BASE + ppn2phys(ppn));

	map_lowhigh(&ksatp, vpn, ppn, pagen, flags);
}

static void map_kstack_lowhigh(void)
{
	/* FIXME: PTE_GLOBAL_SHIFT? */
	unsigned long flags = VMA_READ | VMA_WRITE | VMA_HUGEPAGES;
	unsigned long pagen = phys2ppn(layout.kstack_size);
	unsigned long ppn = phys2ppn(layout.phys_end);
	unsigned long vpn = virt2vpn(VIRT_MEM_KERNEL_BASE + ppn2phys(ppn));

	map_lowhigh(&ksatp, vpn, ppn, pagen, flags);
}

static void map_io_lowhigh(void)
{
	/* FIXME: PTE_GLOBAL_SHIFT? */
	unsigned long flags = VMA_READ | VMA_WRITE | VMA_HUGEPAGES;
	unsigned long pagen = phys2ppn(layout.kelf_base);
	unsigned long ppn = phys2ppn(0);
	unsigned long vpn = virt2vpn(VIRT_MEM_KERNEL_BASE);

	map_lowhigh(&ksatp, vpn, ppn, pagen, flags);
}

/*
 * pt_jump_to_high: jumps from low to high memory, after which the low mappings
 * can be removed
 *
 * Did you know that Jump (For My Love) was written by the Pointer Sisters?
 */
void __attribute((noinline)) pt_jump_to_high(void)
{
	/* FIXME: get rid of 0x1ffff */
	asm volatile("mv t2, ra\n\t" /* Save return address (jalr changes it) */
		     "li t0, 0x1ffff\n\t" /* Add VIRT_MEM_KERNEL_BASE ... */
		     "slli t0, t0, %0\n\t"
		     "auipc t1, 0x0\n\t" /* ... to pc */
		     "add t1, t1, 0x10\n\t" /* Add extra to get past jalr */
		     "add t1, t0, t1\n\t"
		     "jalr t1\n\t"
		     "add ra, t2, t0\n\t" /* Update return address */
		     "add sp, sp, t0\n\t" /* Update stack pointer */
		     "add s0, s0, t0\n\t" /* Update frame pointer */
		     //  /* Update interrupt handler */
		     //  /* FIXME: update mtvec as well, need M-mode for that */
		     "la t0, trap_handler_s\n\t"
		     "csrw stvec, t0\n\t" ::"I"(VIRT_MEM_KERNEL_SHIFT)
		     : "t0", "t1", "t2");
}

/*
 * See pt.h
 */
struct vma *vpn2vma(struct node *vmas_head, unsigned long vpn)
{
	struct node *node = vmas_head->next;

	while (node != vmas_head) {
		struct vma *vma = member_to_struct(node, node, struct vma);

		if (vpn >= vma->vpn && vpn < vma->vpn + vma->pagen) return vma;

		node = node->next;
	}

	return NULL;
}

/*
 * pt_migrate_kvmas: migrates kernel VMAs to +@offset, needed before jumping
 * high
 */
static void pt_migrate_kvmas(unsigned long offset)
{
	/* Don't forget the head! */
	kvmas_head = (struct node *)((char *)kvmas_head + offset);
	node_migrate(kvmas_head, offset);

	struct node *node = kvmas_head->next;

	while (node != kvmas_head) {
		struct vma *kvma = member_to_struct(node, node, struct vma);

		node_migrate(&kvma->node, offset);

		node = node->next;
	}
}

void pt_destroy_low_kvmas(void)
{
	struct node *node = kvmas_head->next;
	struct node *next = node->next;

	while (node != kvmas_head) {
		struct vma *kvma = member_to_struct(node, node, struct vma);

		/* We might remove the node from the list! */
		next = node->next;

		if (kvma->vpn < virt2vpn(VIRT_MEM_KERNEL_BASE)) {
			pt_vma_destroy(&ksatp, kvma);
		}

		node = next;
	}
}

inline void pt_flush_tlb(void)
{
	asm volatile("sfence.vma zero, zero\n\t"
		     "fence\n\t"
		     "fence.i\n\t" ::
			     : "memory");
}

void pt_init(void)
{
	/*
	 * phys2virt is a function pointer, the assignment below means: from
	 * now on, if phys2virt is called, actually call phys2phys. As you can
	 * see below, we change it later, and make phys2virt an alias for
	 * phys2highkvirt.
	 *
	 * Function pointers are an advanced C concept that may take a while to
	 * understand. Just think of phys2virt as an "alias" for whatever
	 * function we make it point to. You won't have to do anything with
	 * function pointers yourself.
	 */
	phys2virt = phys2phys;

	/*
	 * Set up the page tables for the kernel, partially at least: allocate
	 * a root page table. After this function returns, the MMU is still
	 * turned off. Our ksatp variable, that we will later write to the satp
	 * register, see ksatp_enable_paging, now points to our newly allocated
	 * page table
	 */
	ksatp_init();

	/*
	 * Strictly speaking, to support virtual memory we only need page
	 * tables. To also support demand paging, however, we will have to do
	 * additional bookkeeping to keep track of what is mapped and with
	 * what permissions. The basic abstraction here is that of a virtual
	 * memory area (VMA): a region of memory that is, let's say, valid for
	 * the application to use.
	 *
	 * Because the number of VMAs will vary, we keep track of them on a
	 * linked list. This function will initialize that linked list. At this
	 * point, there's just one linked list for all VMAs.
	 */
	pt_init_vmas_head(&kvmas_head);

	/*
	 * That's it for the little bit of initialization. We can now start the
	 * real work: create page tables and set up VMAs.
	 *
	 * Map all physical memory, create a low and a high mapping. The low
	 * mapping will only be used right after the MMU has been turned on and
	 * until we jump to the high one. We destroy the low mappings in
	 * pt_destroy_low_kvmas, see main.c
	 */
	printstr("About to create the first mapping...\n");
	map_phys_mem_lowhigh();
	printstr("First mapping created\n");

	/*
	 * Same as above, but for the kernel stack
	 */
	map_kstack_lowhigh();

	/*
	 * Same as above, but for the kernel's code
	 */
	map_kelf_lowhigh();

	/*
	 * Same as above, but for our little bit of IO, the driver that we use
	 * to print to the screen
	 */
	map_io_lowhigh();

	printstr("About to turn on the MMU...\n");

	/*
	 * Turn on the MMU
	 */
	printstr("About to turn on the MMU...\n");
	ksatp_enable_paging();
	printstr("MMU turned on\n");

	printstr("MMU turned on\n");

	/*
	 * From this point on, to "access" a physical address, which we need
	 * for updating page tables, because page tables only store physical
	 * addresses, we need to convert them into a "high" kernel virtual
	 * address. Hence, update our phys2virt function pointer to do just
	 * that. Note the + VIRT_MEM_KERNEL_BASE here. Why do we need it? (Not
	 * so easy to see.)
	 */
	phys2virt = VIRT_MEM_KERNEL_BASE + phys2highkvirt;

	/*
	 * If your code survives ksatp_enable_paging, and reaches these lines,
	 * congratulations, that was the hard part!
	 */
	buddy_migrate(VIRT_MEM_KERNEL_BASE);

	/*
	 * Finally, update the pointers inside the linked list, they now need
	 * to point to "high" memory
	 */
	pt_migrate_kvmas(VIRT_MEM_KERNEL_BASE);
}

inline void unset_vma_protection()
{
	uint64_t t1;
	csrr(sstatus, t1);
	t1 |= SSTATUS_SUM;
	csrw(sstatus, t1);
}

inline void set_vma_protection()
{
	uint64_t t1;
	csrr(sstatus, t1);
	t1 &= !SSTATUS_SUM;
	csrw(sstatus, t1);
}

#ifdef PT_DEBUG
/*
 * pt_pte_print: prints the fields of a PTE, use it for debugging!
 */
void pt_pte_print(unsigned long pte)
{
	printdbg("reser: ", (void *)((pte >> 53) & 0x3ff));
	printdbg("ppn  : ", (void *)pte_get_ppn(pte));
	printdbg("(phy): ", (void *)ppn2phys(pte_get_ppn(pte)));
	printdbg("rsw  : ",
		 (void *)((pte >> __PTE_RSW_SHIFT) & __PTE_RSW_MASK));
	printdbg("dirty: ", (void *)pte_get(pte, PTE_DIRTY_SHIFT));
	printdbg("acc. : ", (void *)pte_get(pte, PTE_ACCESSED_SHIFT));
	printdbg("glob.: ", (void *)pte_get(pte, PTE_GLOBAL_SHIFT));
	printdbg("user : ", (void *)pte_get(pte, PTE_USER_SHIFT));
	printdbg("exec : ", (void *)pte_get(pte, PTE_EXEC_SHIFT));
	printdbg("write: ", (void *)pte_get(pte, PTE_WRITE_SHIFT));
	printdbg("read : ", (void *)pte_get(pte, PTE_READ_SHIFT));
	printdbg("valid: ", (void *)pte_get(pte, PTE_VALID_SHIFT));
}
#endif

/*
 * pt_print: "page table" print, extremely useful for debugging!
 *
 * Will print the mappings under the kernel's address space, prints for each
 * region the level of the pages in that region, adds a "---- ----" if the
 * level of the pages changes
 *
 * FIXME: this one is ugly
 */
static void pt_print(long user_vpn_to_skip_to)
{
	unsigned long ptes[PT_NUM_LEVELS] = { 0 };
	long prev = -1;
	unsigned long *pte;
	long depth;

	/* -1 means go to kernel directly */
	unsigned long vpn = user_vpn_to_skip_to >= 0 ?
				    user_vpn_to_skip_to :
				    virt2vpn(VIRT_MEM_KERNEL_BASE);

	while (vpn <= virt2vpn(VIRT_MEM_KERNEL_BASE + layout.phys_end +
			       layout.kstack_size)) {
		memset(ptes, 0, sizeof(ptes));

		depth = pt_get_pte(&ksatp, vpn, ptes, &pte);

		/* Page size changes within mapping */
		if (vpn != user_vpn_to_skip_to && depth < 0 && depth != prev) {
			printdbg("---- ---- ", (void *)vpn2virt(vpn));
			printstr("\n\n");
		} else if (depth >= 0 && depth != prev) { /* New mapping */
			printdbg("---- ---- ", (void *)vpn2virt(vpn));
			printdbg("    |     ", (void *)depth2level(depth));
			printstr("    |     \n");
		}

		/* Need to make sure we were not in a mapping */
		if (prev < 0 && vpn > user_vpn_to_skip_to + 256 * PAGE_SIZE &&
		    vpn < virt2vpn(VIRT_MEM_KERNEL_BASE)) {
			/* Jump to just before kernel base */
			vpn = virt2vpn(VIRT_MEM_KERNEL_BASE);
		} else if (depth < 0) {
			vpn += level2pagen(0);
		} else {
			vpn += level2pagen(depth2level(depth));
		}

		prev = depth;
	}
}

/*
 * get_kvirt: returns the kernel virtual address (from the identity map) that
 * maps to the same physical page as the specified user virtual address
 *
 * @satp pointer to the user satp
 * @user_virtual user virtual address
 */
inline void *get_kvirt(unsigned long *satp, void *user_virtual)
{
	unsigned long ptes[PT_NUM_LEVELS];
	unsigned long *pte_out;
	int pt_index;

	pt_index = pt_get_pte(satp, virt2vpn((unsigned long)user_virtual), ptes,
			      &pte_out);

	if (pt_index < 0) {
		printerr(
			"can't find the phys. address of the app's .text segment\n");
		return NULL;
	}

	return phys2virt(ppn2phys(pte_get_ppn(ptes[pt_index])));
}