#include "buddy.h"

#define PHYS_MEM_ORDER 29 /* 512 MB */
#define PHYS_MEM_SIZE (1UL << PHYS_MEM_ORDER)

#define PHYS_MEM_PAGE_ORDER 12
#define PHYS_MEM_PAGE_SIZE (1UL << PHYS_MEM_PAGE_ORDER)

#define PHYS_MEM_START 0x80000000
#define PHYS_MEM_END (PHYS_MEM_START + PHYS_MEM_SIZE)
#define PHYS_MEM_KERNEL_STACK_SIZE (10 * PHYS_MEM_PAGE_SIZE)

/* Buddy-specific macros */
#define BUDDY_MAX_ORDER (PHYS_MEM_ORDER - PHYS_MEM_PAGE_ORDER)

/* | 0x80000000                        |                        |             |
 * | .text segments... .text of buddy  | buddy's .data and .bss | -->         |
 * |                                   |                     <-- kernel stack |
 */

/* __ppn is buddy's internal ppn, with __ppn == 0 for PHYS_MEM_START */
#ifndef phys2ppn
#define phys2ppn(phys) ((phys) >> PHYS_MEM_PAGE_ORDER)
#endif

#define phys2__ppn(phys) (((phys)-PHYS_MEM_START) >> PHYS_MEM_PAGE_ORDER)

#define __ppn2ppn(__ppn) ((__ppn) + phys2ppn(PHYS_MEM_START))
#define ppn2__ppn(ppn) ((ppn)-phys2ppn(PHYS_MEM_START))

#define __ppn2block(__ppn) ((__ppn) + buddy_blocks)
#define block2__ppn(block) ((block)-buddy_blocks)

#define phys2block(phys) __ppn2block(phys2__ppn(phys))

/*
 * TODO: start by implementing block2buddy(block, order), where order is the
 * order of block. Use __ppn2block and block2__ppn
 */

// convert block to ppn, flip bit, convert flipped ppn back to block
#define block2buddy(block, order)                                              \
	(__ppn2block((1UL << order) ^ block2__ppn(block)))

#define is_list_empty(order) (!(buddy_free_lists[order]))
#define is_block_free(block) (!(block->refcnt))

static struct block *buddy_free_lists[BUDDY_MAX_ORDER + 1];
static struct block buddy_blocks[1UL << BUDDY_MAX_ORDER];

struct buddy_layout layout;

extern unsigned long phys_base;

static struct block *buddy_pop(unsigned order)
{
	if (is_list_empty(order)) return NULL;

	struct block *head = buddy_free_lists[order];
	buddy_free_lists[order] = head->next; // detach head
	head->next = NULL; // prevent dangling pointer
	return head;
}

static struct block *buddy_remove(struct block *block, unsigned order)
{
	if (is_list_empty(order)) return NULL;

	if (buddy_free_lists[order] != block) {
		struct block *head = buddy_free_lists[order];
		struct block *tail = head;
		struct block *prev = NULL;

		while (tail->next) {
			if (tail->next == block) prev = tail;
			tail = tail->next;
		}

		if (!prev) return NULL; /* Could not find block */

		buddy_free_lists[order] = block;
		prev->next = NULL;
		tail->next = head;
	}

	return buddy_pop(order);
}

/**
* @brief insert the block in front of the linked list it belongs to
*/
static void buddy_push(struct block *block, unsigned order)
{
	// put existing linked list behind head
	block->next = buddy_free_lists[order];

	// set block as the head of linked list
	buddy_free_lists[order] = block;
}

static int __buddy_find_smallest_free_order(unsigned order)
{
	for (int i = order; i <= BUDDY_MAX_ORDER; i++) {
		if (!is_list_empty(i)) return i;
	}
	return -1;
}

static int __buddy_split(unsigned order)
{
	if (order == 0) return -1;

	struct block *block = buddy_pop(order);

	if (!block) return -2;

	// find the block that will end up being the split's buddy
	struct block *buddy = block2buddy(block, (order - 1));

	block->order -= 1;
	buddy->order -= 1;

	buddy_push(block, (order - 1));
	buddy_push(buddy, (order - 1));

	return 0;
}

static int buddy_split(unsigned smallest_free_order, unsigned desired_order)
{
	/* Will do nothing if smallest_free and desired are equal */
	for (int i = smallest_free_order; i > desired_order; i--) {
		int ret = __buddy_split(i);
		if (ret) return ret;
	}
	return 0;
}

static struct block *__buddy_merge(struct block *block, struct block *buddy)
{
	struct block *rm_block = buddy_remove(block, block->order);
	struct block *rm_buddy = buddy_remove(buddy, buddy->order);

	rm_block->order += 1;
	rm_buddy->order += 1;

	/* select the block pointer with larger address. in free_lists,
	blocks with higher adresses are at front, lower adresses are at back*/
	if (!(block2__ppn(rm_block) > block2__ppn(rm_buddy))) {
		rm_block = rm_buddy;
	}

	buddy_push(rm_block, rm_block->order); // push to free list
	return rm_block;
}

static void __buddy_try_merge(struct block *block)
{
	if (block->order == BUDDY_MAX_ORDER) return;

	struct block *buddy = block2buddy(block, block->order);

	if (block->order != buddy->order) return;

	if (is_block_free(block) && is_block_free(buddy)) {
		block = __buddy_merge(block, buddy);
		__buddy_try_merge(block);
	}
}

int buddy_free(struct block *block)
{
	switch (block->refcnt) {
	case 0:
		return -1; /* Double free */
	case 1:
		block->refcnt = 0;

		buddy_push(block, block->order);
		__buddy_try_merge(block);

		return 0;
	default:
		// block still has references holding on to it, so decrease counter
		block->refcnt -= 1;
		return 0;
	}

	return -1;
}

/* NOTE: this might not give aligned pages? */
struct block *buddy_alloc(unsigned order)
{
	/* Order too large? */
	if (order > BUDDY_MAX_ORDER) return NULL;

	/* TODO: call __buddy_find_smallest_free_order here */
	int smallest_free = __buddy_find_smallest_free_order(order);

	/*
	 * TODO: think about what __buddy_find_smallest_free_order might
	 * return and what should happen in each case (return early upon a
	 * failure)
	 */
	if (smallest_free == -1) return NULL;

	// split block of size smallest free until there is one of size "order"
	buddy_split(smallest_free, order);

	struct block *ret = buddy_pop(order); // fetch the free block
	ret->refcnt += 1; // mark as used
	return ret;
}

struct block *ppn2block(unsigned long ppn)
{
	if (ppn < phys2ppn(PHYS_MEM_START) || ppn >= phys2ppn(PHYS_MEM_END)) {
		return NULL;
	} else {
		return __ppn2block(ppn2__ppn(ppn));
	}
}

unsigned long block2ppn(struct block *block)
{
	return __ppn2ppn(block2__ppn(block));
}

static void buddy_layout_init()
{
	layout.kelf_base = PHYS_MEM_START;
	layout.phys_base = phys_base; /* This one moves, as the kernel grows */
	layout.phys_end = PHYS_MEM_END - PHYS_MEM_KERNEL_STACK_SIZE;
	layout.kstack_size = PHYS_MEM_KERNEL_STACK_SIZE;
}

/**
* @brief: Initialize free linked list table of order 0 to contain entire memory range
*/
static void __buddy_init(void)
{
	phys_base = (unsigned long)&phys_base;

	struct block block = { .refcnt = 1, .order = 0, .next = NULL };

	for (int i = 0; i < ARRAY_SIZE(buddy_blocks); i++) {
		buddy_blocks[i] = block;
	}

	for (unsigned long phys = phys_base;
	     phys < PHYS_MEM_END - PHYS_MEM_KERNEL_STACK_SIZE;
	     phys += PHYS_MEM_PAGE_SIZE) {
		/* TODO: fix buddy_free to make initialization work */
		buddy_free(phys2block(phys));
	}
}

static void buddy_reset(void)
{
	memset(buddy_free_lists, 0,
	       sizeof(struct block *) * ARRAY_SIZE(buddy_free_lists));
	memset(buddy_blocks, 0,
	       sizeof(struct block) * ARRAY_SIZE(buddy_blocks));
	__buddy_init();
}

void buddy_migrate(unsigned long offset)
{
	for (int i = 0; i < ARRAY_SIZE(buddy_free_lists); i++) {
		if (!buddy_free_lists[i]) continue;

		buddy_free_lists[i] =
			(struct block *)((char *)buddy_free_lists[i] + offset);
	}

	for (int i = 0; i < ARRAY_SIZE(buddy_blocks); i++) {
		struct block *block = &buddy_blocks[i];
		if (block->next) {
			block->next =
				(struct block *)((char *)block->next + offset);
		}
	}
}

void buddy_init(void)
{
	__buddy_init();

	buddy_layout_init();

	printstr("--- buddy layout ---\n");
	printdbg("PHYS_MEM_START       : ", layout.kelf_base);
	printdbg("phys_base            : ", layout.phys_base);
	printdbg("phys_end             : ", layout.phys_end);
	printdbg("PHYS_MEM_END         : ", PHYS_MEM_END);
	printdbg("kstack_size          : ", layout.kstack_size);

	printstr("--- buddy meta data ---\n");
	printdbg("buddy_free_lists base: ", buddy_free_lists);
	printdbg("buddy_free_lists end : ",
		 (char *)buddy_free_lists + sizeof(buddy_free_lists));
	printdbg("buddy_blocks base    : ", buddy_blocks);
	printdbg("buddy_blocks end     : ",
		 (char *)buddy_blocks + sizeof(buddy_blocks));
}
