#ifndef BUDDYK_H
#define BUDDYK_H

#include "types.h"
#include "util.h"

struct buddy_layout {
	unsigned long kelf_base;
	unsigned long phys_base;
	unsigned long phys_end; /* Also kstack_top */
	unsigned long kstack_size;
};

struct block {
	unsigned refcnt;
	unsigned order; /* 0 <= order <= BUDDY_MAX_ORDER */
	struct block *next;
};

void buddy_init(void);
void buddy_migrate(unsigned long offset);

struct block *buddy_alloc(unsigned order);
int buddy_free(struct block *block);

struct block *ppn2block(unsigned long ppn);
unsigned long block2ppn(struct block *block);

#endif
